class convertToDegree:
	def __init__(self, lat, lon):
		self.__lat = lat
		self.__lon = lon
		
	@property
	def latitude(self):
		return self.__lat
	
	@latitude.setter
	def latitude(self, lat):
		self.__lat = lat
		
	@property
	def longitude(self):
		return self.__lon
	
	@longitude.setter
	def longitude(self, lon):
		self.__lon = lon
					
	def removeLeadingZero(self, degreeString):
		if degreeString[0]=='0':
			return(degreeString.replace('0', '', 1))
		else:
			return(degreeString)	
	
	def calculateDegree(self, degreeString):
		degreeString = self.removeLeadingZero(degreeString)
		degreeFloat =  float(degreeString[0:2]) + float(degreeString[3:5])/60 + float(degreeString[6:])/3600
	
		return(degreeFloat)

	def convertLatLonToDegree(self):
		latList = []
		lonList = []
		for i in range(len(self.latitude)):
			#print(i)
			#latList.append(float(lat[i][0:2]) + float(lat[i][3:5])/60 + float(lat[i][6:])/3600)
			latList.append(self.calculateDegree(self.latitude[i]))
			#lonList.append(float(lon[i][1:3]) + float(lon[i][4:6])/60 + float(lon[i][7:])/3600)
			lonList.append(self.calculateDegree(self.longitude[i]))
		return latList, lonList
