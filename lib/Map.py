import matplotlib.pyplot as plt
import geopandas as gpd

class Map:
	# Constructor for the 4 class attributes
	def __init__(self, data, latitudeName, longitudeName, variableName):
		self.__data = data
		self.__latitudeName = latitudeName
		self.__longitudeName = longitudeName
		self.__variableName = variableName
	
	# Define the getter and setter functions for all attributes, except for __data		
	def __getLatitudeName(self):
		return(self.__latitudeName)
		
	def __getLongitudeName(self):
		return(self.__longitudeName)
	
	def __getVariableName(self):
		return(self.__variableName)
		
	def __setLatitudeName(self, latitudeName):
		self.__latitudeName = latitudeName
	
	def __setLongitudeName(self, longitudeName):
		self.__longitudeName = longitudeName
		
	def __setVariableName(self, variableName):
		self.__variableName = self.__checkSetter(variableName)
	
	# Check if the variable name of the setter exists in the data	
	def __checkSetter(self, name):
		colNames = list(self.__data.columns)
		elementCount = colNames.count(name)
		if elementCount == 0:
			raise NameError("The variable string '" + name + "'  does not exist in the data frame!")
		else:
			return name	
	
	# Define the properties from the getter and setter functions	
	latitudeName = property(__getLatitudeName, __setLatitudeName)
	longitudeName = property(__getLongitudeName, __setLongitudeName)
	variableName = property(__getVariableName, __setVariableName)	
		
	def convertToGeoDataFrame(self, crsCode):
		geometry = gpd.points_from_xy(y=self.__data[self.latitudeName], x=self.__data[self.longitudeName], crs = crsCode)
		geoDataFrame = gpd.GeoDataFrame(self.__data, geometry=geometry)
		return(geoDataFrame)	
		
	def mapPlotter(self, geoMap, colorCode, labelName, title):
		crsCode = geoMap.crs
		geoDataFrame = self.convertToGeoDataFrame(crsCode=crsCode)
		
		ax = plt.subplot()
		base = geoMap.plot(ax=ax,color="white", edgecolor="black")
		points = geoDataFrame.plot(ax=base, markersize=50, column=self.variableName, cmap=colorCode, legend=True, legend_kwds={"label":labelName, "orientation":"vertical"})	
		ax.axis("off") 
		plt.title(title)
		plt.show()
		
	def mapPlotterCategorical(self, geoMap, colorCode, labelName, title):
		crsCode = geoMap.crs
		geoDataFrame = self.convertToGeoDataFrame(crsCode=crsCode)
		
		ax = plt.subplot()
		base = geoMap.plot(ax=ax,color="white", edgecolor="black")
		points = geoDataFrame.plot(ax=base, markersize=50, column=self.variableName, cmap=colorCode, legend=True, categorical = True)#, legend_kwds={"label":labelName, "orientation":"vertical"})	
		ax.axis("off") 
		plt.title(title)
		plt.show()	
