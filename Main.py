#Import Python packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import geopandas as gpd

# Import custom classes amd functions
from lib.Map import *
from lib.DegreeConverter import convertToDegree

# Read the shapefile of Khozestan, without Regions
#kuhzestanMap = gpd.read_file("./input/KHOZESTAN_1/KHOZESTAN_1.shp")

# Read the GPKG-file, with regions
kuhzestanMap = gpd.read_file("./input/Khozestan_cities.gpkg")

# Read the soil data
soilData = pd.read_csv("../SOIL DATA.csv")

# Convert the Coordinates from string to float and from DD-MM-SS to DD.DD
locations = convertToDegree(soilData["Lat   N"], soilData["Long   E"])
convertedLocations = locations.convertLatLonToDegree()

soilData["LatN"] = convertedLocations[0]
soilData["LonE"] = convertedLocations[1]

# Print the head of the soil Data table for checking
print(soilData.head())

# Create an insatnce of the Map for the PH value and plot the map
khozestanPH = Map(soilData, "LatN", "LonE", "pH")
khozestanPH.mapPlotter(kuhzestanMap, "plasma", "pH value", "Distribution of observed soil pH in Khozestan")

# Create an insatnce of the Map for Nitrogen and plot the map
khozestanN = Map(soilData, "LatN", "LonE", "%T.N.V")
khozestanN.mapPlotter(kuhzestanMap, "plasma", "Nitrogen Content", "Distribution of observed soil nitrogen content in Khozestan")

# Create an insatnce of the Map for organic Carbon and plot the map
khozestanOC = Map(soilData, "LatN", "LonE", "%O.C")
khozestanOC.mapPlotter(kuhzestanMap, "plasma", "organic crabon Content", "Distribution of observed soil organic carbon content in Khozestan")

# Create an insatnce of the Map for phosphorus and plot the map
khozestanP = Map(soilData, "LatN", "LonE", "ave-P")
khozestanP.mapPlotter(kuhzestanMap, "plasma", "Phosphorus", "Distribution of observed soil phosphorus in Khozestan")

# Create an insatnce of the Map for potassium and plot the map
khozestanK = Map(soilData, "LatN", "LonE", "ave-K")
khozestanK.mapPlotter(kuhzestanMap, "plasma", "Potassium", "Distribution of observed soil potassium in Khozestan")

# Create an insatnce of the Map for Zink and plot the map
khozestanZN = Map(soilData, "LatN", "LonE", "Zn")
khozestanZN.mapPlotter(kuhzestanMap, "plasma", "Zink", "Distribution of observed soil zink in Khozestan")

# Create an insatnce of the Map for Manganese and plot the map
khozestanMN = Map(soilData, "LatN", "LonE", "Mn")
khozestanMN.mapPlotter(kuhzestanMap, "plasma", "Manganese", "Distribution of observed soil manganese in Khozestan")

# Create an insatnce of the Map for Iron and plot the map
khozestanFE = Map(soilData, "LatN", "LonE", "Fe")
khozestanFE.mapPlotter(kuhzestanMap, "plasma", "Iron", "Distribution of observed soil iron in Khozestan")

# Create an insatnce of the Map for copper and plot the map
khozestanCU = Map(soilData, "LatN", "LonE", "Cu")
khozestanCU.mapPlotter(kuhzestanMap, "plasma", "Copper", "Distribution of observed soil copper in Khozestan")

# Create an insatnce of the Map for clay content and plot the map
khozestanClay = Map(soilData, "LatN", "LonE", "%CLAY")
khozestanClay.mapPlotter(kuhzestanMap, "plasma", "Clay content", "Distribution of observed clay content in Khozestan")

# Create an insatnce of the Map for silt content and plot the map
khozestanSilt = Map(soilData, "LatN", "LonE", "%SILT")
khozestanSilt.mapPlotter(kuhzestanMap, "plasma", "Silt content", "Distribution of observed silt content in Khozestan")

# Create an insatnce of the Map for clay content and plot the map
khozestanSand = Map(soilData, "LatN", "LonE", "%SAND")
khozestanSand.mapPlotter(kuhzestanMap, "plasma", "Sand content", "Distribution of observed sand content in Khozestan")

# Create an insatnce of the Map for texture and plot the map
khozestanTexture = Map(soilData, "LatN", "LonE", "SOIL-TEX")
khozestanTexture.mapPlotterCategorical(kuhzestanMap, "Set1", "Soil texture", "Distribution of observed soil texture in Khozestan")
